\documentclass[tikz, border=3pt]{standalone}
\usetikzlibrary{decorations.pathreplacing}
\usepackage[outline]{contour}

\begin{document}
\begin{tikzpicture}

\node[scale=5, text width=2.5cm] at (-14.2, -0.5) {Modulated zigzag structure};

\draw[dashed] (-7, -0.5) -- (41, -0.5);

\draw[latex-latex, line width=4pt] (-7, -8.5)  -- node[below, scale=5] {$L / d$} (41., -8.5);

\draw[line width=5pt,
          decoration={border, segment length=10pt, amplitude=30pt, angle=60},
          postaction={decorate,draw, line width=1.5pt}] (-7, -9) -- (-7, 5);

% draw circle 1
\draw[line width=2pt] (-4, 0) circle (3);
\draw[fill] (-4, 0) circle (0.1);

% draw circle 2
\draw[line width=2pt] (1.95, -1.) circle (3);
\draw[fill] (1.95, -1.) circle (0.1);

\draw[line width=3pt] (-4, 0) -- (1.95, -1.0);

\draw[dashed, line width=3pt] (1.95, -1.0) -- (6., -0.4);
\draw[dashed, line width=3pt] (8.5, -0.2) -- (12.5, 1.8);

% draw circle n - 1
\draw[line width=2pt] (12.5, 1.8) circle (3);
\draw[fill] (12.5, 1.8) circle (0.1);

% draw circle n
\draw[line width=2pt] (16.1, -3) circle (3);
\draw[fill] (16.1, -3) circle (0.1);

% draw circle n + 1
\draw[line width=2pt] (20.4, 1.2) circle (3);
\draw[fill] (20.4, 1.2) circle (0.1);

\draw[line width=3pt] (12.5, 1.8) -- (16.1, -3);

\draw[line width=3pt] (16.1, -3) -- (20.4, 1.2);

\draw[-latex, line width=7pt, color=red] (16.1, -3) -- (16.1, -0.5);
\node[scale=5.5, color=red] at (18, -4.5) {$F_n = -r_n$};

%\draw[-latex, line width=5pt, color=red] (12.5, 1.8) -- (12.5, -0.5);
%\node[scale=4, color=red] at (12.5, 2.8) {$F_{n - 1}$};

%\draw[-latex, line width=5pt, color=red] (20.4, 1.2) -- (20.4, -0.5);
%\node[scale=4, color=red] at (20.4, 2.1) {$F_{n + 1}$};

\draw[dashed, line width=3pt] (20.4, 1.2) -- (25.2, -0.8);
\draw[dashed, line width=3pt] (28, -0.4) -- (32.0, -1);

%\node[scale=4.5] at (16.1, -7) {$n$};


% draw circle N - 1
\draw[line width=2pt] (32., -1) circle (3);
\draw[fill] (32., -1) circle (0.1);

% draw circle N
\draw[line width=2pt] (38., -0.1) circle (3);
\draw[fill] (38., -0.1) circle (0.1);


\draw[line width=3pt] (32., -1) -- (38., -0.1);
\node[scale=4.5] at (38, -3.8) {$N$};


\draw[line width=5pt,
          decoration={border, segment length=10pt, amplitude=30pt, angle=-60},
          postaction={decorate,draw, line width=1.5pt}] (41.1, -9) -- (41.1, 5);
      
\node[scale=5, text width=2.5cm] at (-11, 12) {linear chain structure};

\draw[dashed] (-7, 12) -- (45, 12);
          
\draw[line width=5pt,
          decoration={border, segment length=10pt, amplitude=30pt, angle=60},
          postaction={decorate,draw, line width=1.5pt}] (-7, 7) -- (-7, 18);


\draw[latex-latex, line width=4pt] (-7, 17)  -- node[above, scale=5] {$Nd$} (45., 17);

% draw circle 1
\draw[line width=2pt] (-4, 12) circle (3);
\draw[fill] (-4, 12) circle (0.1);      

% draw circle 2
\draw[line width=2pt] (2, 12) circle (3);
\draw[fill] (2, 12) circle (0.1);

\draw[line width=3pt] (-4, 12) -- (2, 12);

\draw[dashed, line width=3pt] (2, 12) -- (6, 12);
\draw[dashed, line width=3pt] (9, 12) -- (13, 12);


% draw circle n-1
\draw[line width=2pt] (13, 12) circle (3);
\draw[fill] (13, 12) circle (0.1);

% draw circle n
\draw[line width=2pt] (19, 12) circle (3);
\draw[fill] (19, 12) circle (0.1);

%\node[scale=4.5] at (19, 8) {$n$};

% draw circle n+1
\draw[line width=2pt] (25, 12) circle (3);
\draw[fill] (25, 12) circle (0.1);

\draw[dashed, line width=3pt] (25, 12) -- (29, 12);
\draw[dashed, line width=3pt] (32, 12) -- (36, 12);

\draw[, line width=3pt] (13, 12) -- (19, 12);
\draw[, line width=3pt] (19, 12) -- (25, 12);

% draw circle N-1
\draw[line width=2pt] (36, 12) circle (3);
\draw[fill] (36, 12) circle (0.1);

% draw circle N
\draw[line width=2pt] (42, 12) circle (3);
\draw[fill] (42, 12) circle (0.1);

\node[scale=4.5] at (42, 8) {$N$};

\draw[, line width=3pt] (36, 12) -- (42, 12);

          
\draw[line width=5pt,
          decoration={border, segment length=10pt, amplitude=30pt, angle=-60},
          postaction={decorate,draw, line width=1.5pt}] (45, 7) -- (45, 18);

\draw[line width=2pt] (45, -9) -- (45, 18);

\draw[line width=2pt, decoration={
        brace, amplitude=20pt, mirror
        },
    decorate
] (41.1, -9) -- node[scale=4.8, below, yshift=-2.5pt] {compression $\Delta$} (45, -9);

\end{tikzpicture}
\end{document}    